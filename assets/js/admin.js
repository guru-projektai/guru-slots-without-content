class AdminSettings {
  constructor() {
    this.bindings = new Bindings()
  }

  init() {
    this.bindings.init()
  }
}

class Bindings {
  loading = false

  init = () => {
    this.setBindings()
  }

  setBindings = () => {
    let that = this

    that.loadData().then(() => {
      that.loading = false
      $('.slots-table .slots-loading').hide()
      $('.indexed-slots-table .indexed-slots-loading').hide()
    })

    $('#fetchSlots').on('click', function (e) {
      if (! that.loading) that.fetchSlots(e)
    })
  }

  fetchSlots(e) {
    let button = $(e.target)
    button.text('Fetching and updating...')

    this.loadData().then(() => {
      this.loading = false
      $('.slots-table .slots-loading').hide()
      $('.indexed-slots-table .indexed-slots-loading').hide()
      button.text('Fetch and update slots without content')
    })
  }

  loadData() {
    this.loading = true
    $('.slots-table .slots-loading').show()
    $('.indexed-slots-table .indexed-slots-loading').show()

    return $.ajax({
      type: 'POST',
      url: 'admin-ajax.php',
      data: {
        action: 'get_post_types_without_content', // load function hooked to: "wp_ajax_*" action hook
      },
      success: result => this.setHtml(result),
      error: () => alert('error')
    })
  }

  setTableHtml(table, data) {
    Object.keys(data).forEach((key, index) => {
      let body = $('<tbody></tbody>')

      let row = $(`
            <tr>
                <td>${ ++index }</td>
                <td>
                    <a href="${ data[key].url }">
                        ${ data[key].url }
                    </a>
                </td>
                <td>${ data[key].hasOwnProperty('post_content') ? data[key].post_content : '...' }</td>
                <td>
                    <a href="${ data[key].edit_link }">
                        Edit
                    </a>
                </td>
            </tr>`)

      body.append(row)

      table.append(row)
    })
  }

  setHtml(result) {
    if (result.success) {
      let withoutContentTable = $('.slots-table tbody').empty()
      let indexedSlotsTable = $('.indexed-slots-table tbody').empty()

      const nonIndexedSlots = Object.keys(result.data.no_content_items).length;
      const indexedSlots = Object.keys(result.data.indexed_items).length

      $('.without-slots').text(nonIndexedSlots)
      $('.non-indexed-count').text(nonIndexedSlots)
      $('.indexed-count').text(indexedSlots)

      $('.total-slots').text(result.data.total_slots)

      this.setTableHtml(withoutContentTable, result.data.no_content_items)
      this.setTableHtml(indexedSlotsTable, result.data.indexed_items)
    } else {
      alert('Something went wrong')
    }
  }
}

jQuery(document).ready(function ($) {
  (new AdminSettings()).init()
})
