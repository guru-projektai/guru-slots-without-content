<?php
/**
 * Plugin Name: Guru Slots Without Content
 * Description: Guru Slots Without Content
 * Version: 1.0.0
 * Author: Mindaugas
 */

if ( !defined( 'ABSPATH' ) ) {
    die;
}

define( 'Guru_Slots_Without_Content_FILE', __FILE__ );
define( 'Guru_Slots_Without_Content_PATH', plugin_dir_path( Guru_Slots_Without_Content_FILE ) );
define( 'Guru_Slots_Without_Content_URL', plugin_dir_url( Guru_Slots_Without_Content_FILE ) );

register_deactivation_hook( Guru_Slots_Without_Content_FILE, array( 'Guru_Slots_Without_Content', 'guru_slots_without_content_deactivate' ) );

final class Guru_Slots_Without_Content
{

    /**
     * Plugin instance.
     *
     * @var Guru_Slots_Without_Content
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return Guru_Slots_Without_Content
     * @static
     */
    public static function get_instance()
    {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Guru_Slots_Without_Content constructor.
     */
    private function __construct()
    {
        register_activation_hook(Guru_Slots_Without_Content_FILE, array($this , 'guru_slots_without_content_activate'));

        $this->guru_slots_without_content_includes();

//        add_action('admin_init', [$this, 'get_post_types_without_content']);
        add_action('admin_menu', [$this, 'guru_slots_without_content_plugin_menu']);
        add_action('admin_notices', [$this, 'guru_slots_without_content_warning']);
        add_action('save_post_vegashero_games', [$this, 'guru_on_save_post_type'], 10, 3);

        add_action('wp_ajax_get_post_types_without_content', [$this, 'get_post_types_without_content']);
        add_action('wp_ajax_nopriv_get_post_types_without_content', [$this, 'get_post_types_without_content']);
    }

    public function guru_on_save_post_type($post_ID, $post, $update)
    {
        $post = json_decode(json_encode($post), true);

        $is_without_content = is_slot_without_content($post);

        self::guru_update_seo_settings_for_page($post_ID, $is_without_content);

        $slots_without_content = get_option('guru_slots_without_content', []);

        if (! $is_without_content) {
            $slots_without_content = array_filter($slots_without_content, static function ($item) use ($post_ID) {
                return $item !== $post_ID;
            });
        } else {
            $slots_without_content[] = $post_ID;
        }

        update_option('guru_slots_without_content', $slots_without_content);
    }

    public function guru_slots_without_content_warning()
    {
        global $post;

        $slots_without_content = get_option('guru_slots_without_content', []);

        if (count($slots_without_content) < 1) return;

        $current_screen = get_current_screen();

        if (
            $post->post_type === 'vegashero_games'
            && $current_screen->base === 'post'
            && in_array($post->ID, $slots_without_content)
        ) {
            echo '<div class="error"><h3 style="color: black;">This page has not been indexed yet, please make sure to index!</h3></div>';
        }
    }

    public function guru_slots_without_content_plugin_menu()
    {
        add_menu_page(
            'Guru slots without content',
            'Guru slots without content',
            'edit_pages',
            'guru-slots-without-content',
            [ $this, 'guru_slots_without_content_plugin_settings_page' ]
        );
    }

    public function guru_slots_without_content_plugin_settings_page()
    {
        wp_enqueue_style('guru-slots-without-content-admin-css', Guru_Slots_Without_Content_URL . 'assets/css/admin.css');
        ob_start();

        include Guru_Slots_Without_Content_PATH . 'templates/admin-page.php';

        echo ob_get_clean();
    }

    /**
     * @return void
     *
     * Filters out posts that doest not have proper description
     */
    public static function get_post_types_without_content()
    {
        try {
            $no_content_items = get_slots_without_content();

            foreach (array_keys($no_content_items) as $no_content_item_id) {
                self::guru_update_seo_settings_for_page($no_content_item_id, true);
            }

            update_option('guru_slots_without_content', array_keys($no_content_items));

            global $wpdb;

            $sql = "SELECT COUNT(*) as slots_count FROM {$wpdb->prefix}posts WHERE post_type = 'vegashero_games' AND post_status = 'publish'";
            $total_slots = $wpdb->get_var($sql);

            $indexed_items = guru_get_indexed_slots(
                implode(',', array_keys($no_content_items))
            );

            update_option('guru_slots_with_content', array_keys($indexed_items));

            wp_send_json_success([
                'no_content_items' => $no_content_items,
                'indexed_items' => $indexed_items,
                'total_slots' => $total_slots,
            ]);
        } catch (Exception $exception) {
            wp_send_json_error('Something went wrong with getting post types without content and updating WPSEAO meta tags' . $exception->getMessage());
        }
    }

    /**
     * @param $post_ID
     * @param $is_without_content
     * Saving the new meta values for Yoast SEO
     */
    public static function guru_update_seo_settings_for_page($post_ID, $is_without_content)
    {
        $value = $is_without_content ? '1' : '0';

        // For saving on post (clicking update button)
        add_action('wpseo_saved_postdata', static function() use ($post_ID, $value) {
            self::update_wpseo($value, $post_ID);
        });

        self::update_wpseo($value, $post_ID);
    }

    private static function update_wpseo($value, $post_ID)
    {
        if (class_exists('WPSEO_Meta')) {
            WPSEO_Meta::set_value( 'meta-robots-noindex', $value, $post_ID );
            WPSEO_Meta::set_value( 'meta-robots-nofollow', $value, $post_ID );
        }
    }


    /**
     * Run when deactivate plugin.
     */
    public static function guru_slots_without_content_deactivate()
    {
        require_once Guru_Slots_Without_Content_PATH . 'includes/guru-slots-without-content-deactivator.php';
        Guru_Slots_Without_Content_Deactivator::deactivate();
    }

    /**
     * Run when activate plugin.
     */
    public function guru_slots_without_content_activate()
    {
        require_once Guru_Slots_Without_Content_PATH . 'includes/guru-slots-without-content-activator.php';
        Guru_Slots_Without_Content_Activator::activate();
    }

    /**
     * Loading plugin functions files
     */
    public function guru_slots_without_content_includes()
    {
        require_once Guru_Slots_Without_Content_PATH . 'includes/guru-slots-without-content-functions.php';
    }
}

function Guru_Slots_Without_Content()
{
    return Guru_Slots_Without_Content::get_instance();
}

$GLOBALS['Guru_Slots_Without_Content'] = Guru_Slots_Without_Content();
