<script type="module" src="<?= Guru_Slots_Without_Content_URL . 'assets/js/admin.js' ?>"></script>
<div class="wrap">
    <h2>Guru slots without content (<span class="without-slots">0</span> of <span class="total-slots">0</span>)</h2>
    <div style="display: flex; justify-content: space-between">
        <p>Pages that has only have description with shortcode (example: [example-shortcode]) or pages that has no
            description at all (blank)</p>
        <div id="fetchSlots" class="button button-primary">Fetch and update slots without content</div>
    </div>
    <div style="display: flex">
        <div class="block-relative slots-table" style="width: 50%">
            <h3>Non indexed slots (<span class="non-indexed-count">0</span>)</h3>
            <div class="slots-loading"></div>
            <table class="widefat fixed ">
                <thead>
                <tr>
                    <th style="width: 50px">ID</th>
                    <th>Url</th>
                    <th>Content</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody class="slots-body"></tbody>
            </table>
        </div>
        <div class="block-relative indexed-slots-table" style="width: 50%">
            <h3>Indexed slots (<span class="indexed-count">0</span>)</h3>
            <div class="indexed-slots-loading"></div>
            <table class="widefat fixed ">
                <thead>
                <tr>
                    <th style="width: 50px">ID</th>
                    <th>Url</th>
                    <th>Content</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody class="indexed-slots-body"></tbody>
            </table>
        </div>
    </div>
</div>
