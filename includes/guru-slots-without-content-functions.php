<?php

function get_slots_without_content()
{
    global $wpdb;
    global $shortcode_tags;

    $sql = "SELECT ID, guid, post_type, post_content FROM {$wpdb->prefix}posts WHERE post_type = 'vegashero_games' AND post_status = 'publish'";

    $no_content_items = [];

    $result = $wpdb->get_results($sql, 'ARRAY_A');

    foreach ($result as $item) {
        if (is_content_empty($item)) {
            $no_content_items[$item['ID']] = generate_item_response($item);
            continue;
        }

        preg_match_all(
            '/' . get_shortcode_regex(array_keys($shortcode_tags)) . '/',
            $item['post_content'],
            $matches,
            PREG_SET_ORDER
        );

        if ($matches) {
            if (is_content_only_shortcodes($item, $matches)) {
                $no_content_items[$item['ID']] = generate_item_response($item);
            } else {
                if (is_trimmed_content($item, $matches)) {
                    $no_content_items[$item['ID']] = generate_item_response($item);
                }
            }
        }
    }

    return $no_content_items;
}

function guru_get_indexed_slots($without_slots_ids)
{
    global $wpdb;

    $sql = "SELECT ID, guid, post_type
            FROM {$wpdb->prefix}posts
            WHERE post_type = 'vegashero_games'
            AND post_status = 'publish'
            AND ID NOT IN ($without_slots_ids)";

    $indexed_items = [];

    $result = $wpdb->get_results($sql, 'ARRAY_A');

    foreach ($result as $item) {
        $indexed_items[$item['ID']] = generate_item_response($item, true);
    }

    return $indexed_items;
}

function generate_item_response($item, $trim_content = false)
{
    $response = [
        'url' => get_permalink($item['ID']),
        'edit_link' => get_edit_post_link($item['ID']),
    ];

    if (! $trim_content) $response['post_content'] = $item['post_content'];

    return $response;
}

function is_slot_without_content($item)
{
    if (is_content_empty($item)) return true;

    global $shortcode_tags;

    preg_match_all(
        '/' . get_shortcode_regex(array_keys($shortcode_tags)) . '/',
        $item['post_content'],
        $matches,
        PREG_SET_ORDER
    );

    if (is_content_only_shortcodes($item, $matches)) return true;

    if (is_trimmed_content($item, $matches)) return true;

    return false;
}

function is_content_empty($item)
{
    return $item['post_content'] === '';
}

function is_content_only_shortcodes($item, $matches)
{
    $containing_content = $item['post_content'];

    foreach ($matches as $match) {
        $containing_content = str_replace($match[0], '', $containing_content);
    }

//    wp_send_json_success([
//        'test' => $shortcodes_length,
//        'asd' => strlen($containing_content)
//    ]);
//    var_dump($shortcodes_length, $containing_content);

    return trim($containing_content) === '';
}

function is_trimmed_content($item, $matches)
{
    $containing_description = $item['post_content'];
    $containing_description = str_replace($matches[0][0], '', $containing_description);

    return trim($containing_description === '');
}
